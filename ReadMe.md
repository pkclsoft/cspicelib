**CSPICELib** - An iOS and macOS port of the CSPICE library from **NAIF**.

## Attribution

This repository makes use of NASA’s SPICE mission geometry system software and data: [https://naif.jpl.nasa.gov](https://naif.jpl.nasa.gov).

## Support

All direct queries regarding this repository and its use should be directed to me, NOT the NAIF team. This repository is the result of my taking a copy of Version 66 of CSPICE, and making the necessary changes to allow the library to be used in an iOS environment.

I follow that with a disclaimer that I am completely reliant on the information provided at:

[https://naif.jpl.nasa.gov/naif/toolkit.html](https://naif.jpl.nasa.gov/naif/toolkit.html)

when it comes to understanding how to use the library.  

If you need help with what I have done to make the library work on iOS, then by all means ask.

## What did I do?

The principle changes that I made were to ensure that all disk I/O is done within the sandbox environment provided to an iOS app.  Above all else, I did not want to change the code generously made available by NAIF any more than absolutely essential.

I added the header: 

    sandbox_interface.h

which provides the essential interaction between CSPICE and the app.  It's a very basic interface, and has deliberately crafted to minimise the changes I made within the CSPICE code itself.

Within this I've provided a mechanism to tell CSPICE where files should be read and written within the file system.

Then, wherever CSPICE did file I/O I changed it to ensure it made use of the documents folder provided by the interface.

In addition to this, there were two system calls to remove and rename files.  I created wrappers for those calls and changed CSPICE to use those wrappers instead.

## How to use the library.

When your app starts, it should first provide CSPICE with the location of the files you want it to load via a call to:

    void  setDocumentsFolder(const  char *str);

The CSPICE library is then able to access the data files from within your apps sandbox.  Calls to the CSPICE library after this then make use of that folder, with an expectation that any files will be found at that location.

## An Example

In the test code for the library, the application uses the main bundle to locate the files to be loaded by the library:

    setDocumentsFolder([[[[NSBundle  mainBundle] pathForResource:@"naif0012"  ofType:@"tls"] stringByDeletingLastPathComponent] cStringUsingEncoding:NSASCIIStringEncoding]);

Then, to locate Mars at the current time, the application calls CSPICE load the necessary data files:

    // first load the data files.
    //
    furnsh_c("naif0012.tls");
    furnsh_c("de432s.bsp");
    
    // now get the position of mars.
    //
    SpiceDouble tdb;
    SpiceDouble lt;
    SpiceDouble position[3];
    
This line gets the current date/time and produces a string in the format expected by CSPICE.

    str2et_c([[self  date2str:[NSDate  date] onlyDate:NO] cStringUsingEncoding:NSASCIIStringEncoding], &tdb);

This call into the CSPICE to request the position of Mars at the date/time specified in `tdb`.
    
    spkpos_c("MARS BARYCENTER", tdb, "J2000", "LT+S", "SUN", position, &lt);
