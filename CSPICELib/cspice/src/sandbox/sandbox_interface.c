//
//  sandbox_interface.c
//
//  Created by Peter Easdown on 25/8/18.
//

#include <stdio.h>

#include <string.h>
#include <stdlib.h>

static char*DOCUMENTS_FOLDER;
static unsigned long DOCUMENTS_FOLDER_LEN;

static char*latestFilePath = NULL;

void setDocumentsFolder(const char *str) {
    DOCUMENTS_FOLDER = malloc(strlen(str)+1);
    strncpy(DOCUMENTS_FOLDER, str, strlen(str));
    DOCUMENTS_FOLDER[strlen(str)] = 0x00; // terminate the string.
    DOCUMENTS_FOLDER_LEN = strlen(str);
    latestFilePath = malloc(1024);
    
    printf("CSPICE setDocumentsFolder to: %s\n", str);
    printf("CSPICE DOCUMENT_FOLDER: %s\n", DOCUMENTS_FOLDER);
    printf("CSPICE DOCUMENT_FOLDER_LEN: %lu\n", DOCUMENTS_FOLDER_LEN);
}

char *getDocumentsFolder() {
    return DOCUMENTS_FOLDER;
}

char* prependDocumentsFolder(char *file, int file_len) {
        
    // If this has already been done, then don't do it again.
    //
    if ((file[0] == '/') || (file[0] == ' ') || (file[0] == 0x0)) {
        return file;
    }
    
    // last check to make sure we don't double up.  some cspice methods end up passing a path to a lower
    // level function that ends up here again.
    //
    if ((file_len > DOCUMENTS_FOLDER_LEN) && (strncmp(DOCUMENTS_FOLDER, file, DOCUMENTS_FOLDER_LEN) == 0)) {
        return file;
    }
    
    unsigned long out_len = strlen(file) + DOCUMENTS_FOLDER_LEN + 1;
    
//    printf("CSPICE prependDocumentFolder to: %s with max length: %d\n", file, file_len);
//    printf("CSPICE strlen(file)(%lu) + DOCUMENT_FOLDER_LEN(%lu) = %lu\n", strlen(file), DOCUMENTS_FOLDER_LEN, out_len);

    strncpy(latestFilePath, DOCUMENTS_FOLDER, DOCUMENTS_FOLDER_LEN);
    latestFilePath[DOCUMENTS_FOLDER_LEN] = '/';

    int nextO = (int)DOCUMENTS_FOLDER_LEN + 1;
    int nextI = 0;

    while ((nextI < file_len) && (file[nextI] != ' ') && (file[nextI] != 0x00)) {
        latestFilePath[nextO] = file[nextI];
        nextO++;
        nextI++;
    }

    latestFilePath[nextO] = 0x00;

//    printf("CSPICE prepended file: %s\n", latestFilePath);

    return latestFilePath;
}

void removeDocumentsFolder(char *file, int file_len) {
    if (file[0] != '/') {
        return;
    }

    int nextO = 0;
    int nextI = (int)DOCUMENTS_FOLDER_LEN + 1; // +1 to skip the separator

    while ((nextI < file_len) && (file[nextI] != ' ') && (file[nextI] != 0x00)) {
        file[nextO] = file[nextI];
        nextO++;
        nextI++;
    }

    while (nextO < file_len) {
        file[nextO] = ' ';
        nextO++;
    }
}

int removeFile(char *file) {
    char *path = prependDocumentsFolder(file, (int)strlen(file));
    return remove(path);
}

int renameFile(char *file, char* toFile) {
//    printf("renameFile from: %s to %s", file, toFile);
    
    char* oldPath_tmp = prependDocumentsFolder(file, (int)strlen(file));

    char oldPath[strlen(oldPath_tmp)];
    strncpy(oldPath, oldPath_tmp, sizeof(oldPath));
    
    char* newPath = prependDocumentsFolder(toFile, (int)strlen(toFile));

    return rename(oldPath, newPath);
}
