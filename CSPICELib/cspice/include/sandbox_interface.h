//
//  sandbox_interface.h
//
//  Created by Peter Easdown on 24/8/18.
//

#ifndef sandbox_interface_h
#define sandbox_interface_h

#include <string.h>


/*!
 *  Inform the CSPICE library where to read and write files from.
 *  @param str a C string containing the path to the folder (within the sandbox) where files should be written and read.
 */
void setDocumentsFolder(const char *str);

/*!
 *  Returns a C string containing the path to the folder (within the sandbox) where files should be written and read.
 */
char *getDocumentsFolder();

/*! This function accepts a filename (and length) and prepends the currently defined documents folder to the C string.
 *  @param file A C string containing a filename.  The buffer containing this string needs to be long enough to allow
 *  the documents folder path.
 *  @param file_len an integer containing the length of the input filename within the C string buffer.
 *  @return a pointer to a C string containing the full pathname, or file if an error occurs.
 */
char* prependDocumentsFolder(char *file, int file_len);

/*! Removes the path of the documents folder from the input string.
 *  @param file A C string containing a pathname that is expected to contain the current documents folder path.
 *  @param file_len the length of the value in the input string.
 */
void removeDocumentsFolder(char *file, int file_len);

/*!
 * Wrapper around the system remove() function that assumes that the file
 * exists within the documents folder.
 */
int removeFile(char *file);

/*!
 * Wrapper around the system rename() function that assumes that the file
 * exists within the documents folder.
 */
int renameFile(char *file, char* toFile);

#endif /* sandbox_interface_h */
